import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  public logeado: boolean;
  constructor(private router: Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      // //si el email es null en el localstorange que no vaya a la ruta correspondinente, de lo contrario si va
      if (JSON.parse(localStorage.getItem('data')) === null){
        // si es null redirigimos al login
        this.router.navigate(['/login']);
        return false;
      }else{
        this.logeado = true;
        return true;
      }
  }

}

