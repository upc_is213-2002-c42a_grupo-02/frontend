import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { CalificarComponent } from './components/calificar/calificar.component';
import { RegistryComponent } from './components/registry/registry.component';
import { UsersComponent } from './components/users/users.component';
import { ProvidersComponent } from './components/providers/providers.component';
import { RegisterproviderComponent } from './components/registerprovider/registerprovider.component';
import { ServicesComponent } from './components/services/services.component';
import { LoginGuard } from './login.guard';
import { NoLoginGuard } from './no-login.guard';
import { IndexComponent } from './components/dashboarduser/index/index.component';
import { PrincipalComponent } from './components/dashboardprovider/principal/principal.component';
import { UpdateperfilComponent } from './components/dashboardprovider/updateperfil/updateperfil.component';
import { CategoryproviderComponent } from './components/categoryprovider/categoryprovider.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    CalificarComponent,
    RegistryComponent,
    UsersComponent,
    ProvidersComponent,
    RegisterproviderComponent,
    ServicesComponent,
    IndexComponent,
    PrincipalComponent,
    UpdateperfilComponent,
    CategoryproviderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [LoginGuard, NoLoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
