import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { CalificarComponent } from './components/calificar/calificar.component';
import { RegistryComponent } from './components/registry/registry.component';
import { UsersComponent } from './components/users/users.component';
import { ProvidersComponent } from './components/providers/providers.component';
import { RegisterproviderComponent } from './components/registerprovider/registerprovider.component';
import { ServicesComponent } from './components/services/services.component';
import { LoginGuard } from './login.guard';
import { NoLoginGuard } from './no-login.guard';
import { IndexComponent } from './components/dashboarduser/index/index.component';
import { PrincipalComponent } from './components/dashboardprovider/principal/principal.component';
import { UpdateperfilComponent } from './components/dashboardprovider/updateperfil/updateperfil.component';
import { CategoryproviderComponent } from './components/categoryprovider/categoryprovider.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent, canActivate: [NoLoginGuard]},
  {path: 'registry', component: RegistryComponent},
  {path: 'users', component: UsersComponent, canActivate: [LoginGuard] },
  {path: 'providers', component: ProvidersComponent, canActivate: [LoginGuard]},
  {path: 'registerprovider', component: RegisterproviderComponent},
  {path: 'calificar', component: CalificarComponent},
  {path: 'services', component: ServicesComponent},
  {path: 'index', component: IndexComponent,canActivate: [LoginGuard]},
  {path: 'principal', component: PrincipalComponent,canActivate: [LoginGuard]},
  {path: 'updateperfil', component: UpdateperfilComponent,canActivate: [LoginGuard]},
  
  {path: 'categoryprovider/:id', component: CategoryproviderComponent},

  {path: '', pathMatch: 'full', redirectTo: 'home'},
  {path: '**', pathMatch: 'full', redirectTo: 'home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
