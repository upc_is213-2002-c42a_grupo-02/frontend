import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RutasService } from '../../services/rutas.service';

@Component({
  selector: 'app-registerprovider',
  templateUrl: './registerprovider.component.html',
  styleUrls: ['./registerprovider.component.scss']
})
export class RegisterproviderComponent implements OnInit {

  forma : FormGroup;

  constructor(private rutas: RutasService, private fb: FormBuilder) {
    this.crearFormulario();
   }

  ngOnInit(): void {
  }

  get nameInvalid() {
    return this.forma.get('name').invalid && this.forma.get('name').touched
  }
  get lastnameInvalid() {
    return this.forma.get('lastname').invalid && this.forma.get('lastname').touched
  }
  get datebirthInvalid() {
    return this.forma.get('datebirth').invalid && this.forma.get('datebirth').touched
  }
  get emailInvalid() {
    return this.forma.get('email').invalid && this.forma.get('email').touched
  }
  get usernameInvalid() {
    return this.forma.get('username').invalid && this.forma.get('username').touched
  }
  get secretInvalid() {
    return this.forma.get('secret').invalid && this.forma.get('secret').touched
  }
  get phoneInvalid() {
    return this.forma.get('phone').invalid && this.forma.get('phone').touched
  }
  get addressInvalid() {
    return this.forma.get('location.address').invalid && this.forma.get('location.address').touched
  }

  crearFormulario() {
    this.forma = this.fb.group({
      username  : ['', Validators.required ],
      email     : ['', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      secret    : ['', Validators.required],
      name      : ['', Validators.required ],
      lastname  : ['', Validators.required],
      datebirth : ['', Validators.required],
      phone     : ['', Validators.required],
      location  : this.fb.group({
        address   : ['', Validators.required]
      }),
    });
  }

  guardar(){
    //console.log(this.forma.value );

    if ( this.forma.invalid ) {
      return Object.values( this.forma.controls ).forEach( control => {
        if ( control instanceof FormGroup ) {
          Object.values( control.controls ).forEach( control => control.markAsTouched() );
        } else {
          control.markAsTouched();
        }
      });
    }
    this.rutas.postProvider(this.forma.value).subscribe(response =>{
      console.log(response);
    });

    this.forma.reset({
      username: '',
      email: '',
      secret: '',
      name: '',
      lastname: '',
      datebirth: '',
      phone: '',
      location: {
        address: '',
      },

    });

  }

}
