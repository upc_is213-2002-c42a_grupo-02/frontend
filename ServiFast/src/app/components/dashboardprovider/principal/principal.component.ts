import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RutasService } from '../../../services/rutas.service';
@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {
  
  forma = new FormGroup({});
  

  provider:any[] = [];
  //Proveedor:any[]=[];

  id;
  proveedor;
  cumpleanios;
  email;
  telefono;
  usuario;
  experiencia;
  descripcion;
  frases;
  nombres;
  apellidos;
  direccion;

   // SERVICIOS OFRECIDOS
   formaCategoria = new FormGroup({});
   listaCategorias: any[] = [];
   listaCategoriesTodas: any[] = [];

  constructor(private rutas:RutasService, private fb: FormBuilder, private router: Router) {
    this.crearFormulario();
    this.crearFormularioCategoria();
   }

  ngOnInit(): void {
    this.provider = JSON.parse(localStorage.getItem('data'));
    this.id = this.provider['id'];
    this.proveedor = this.provider['name'] + ' ' + this.provider['lastname'];
    this.cumpleanios = this.provider['datebirth'];
    this.email = this.provider['email'];
    this.telefono = this.provider['phone'];
    this.usuario = this.provider['username'];
    this.experiencia = this.provider['experience'];
    this.descripcion = this.provider['about'];
    this.frases = this.provider['keyphrases'];
    this.nombres = this.provider['name'];
    this.apellidos = this.provider['lastname'];
    this.direccion = this.provider['location'];
    
    // SERVICIOS OFRECIDOS
    this.listCategoryProvider(this.id);
    this.todasCategorias();

    //console.log(this.id);
  }

  
  editar(Id:number){ 

    this.rutas.getCategoryProvider(Id).subscribe(data=>{
      console.log(data['location'].address)
      this.forma = this.fb.group({
        id: data['id'],
        username: data['username'],
        name: data['name'],
        lastname: data['lastname'],
        email:data['email'],
        phone: data['phone'],
        about: data['about'],
        experience:data['experience'],
        keyphrases: data['keyphrases'],
        secret: data['secret'],
        datebirth: data['datebirth'],
        location: this.direccion
      });
    })
  }

  crearFormulario() {
    this.forma = this.fb.group({
      id         : ['',],
      username   : ['',],
      name       : ['',],
      lastname   : [''],
      email      : ['',],
      phone      : ['',],
      about      : [''],
      experience : ['',],
      keyphrases : ['',],
      secret     : ['',],
      datebirth : [''],
      location: this.fb.group({
        address: [''],
      })
    });
  }

  actualizar(){
    this.rutas.putProvider(this.forma.value).subscribe(response =>{
      console.log(response);
      
      if(response != null){
        //alert('Datos actualizados');
        //cerrar el modal
        document.getElementById('cerrarModal').click();
        document.getElementById('mostrarExito').click();
      }else{
        alert('Error!. Intentelo nuevamente');
      }
    });
  }

  logout(){
    localStorage.removeItem('data');
    localStorage.removeItem('tipo');
    this.router.navigate(['/login']);
    
  }

  // SERVICIOS OFRECIDOS
  todasCategorias(){
    this.rutas.getCategories()
    .subscribe((data: any)=> {
      this.listaCategoriesTodas = data
      console.log(this.listaCategoriesTodas)
    });
  }

  listCategoryProvider(id: number){
    this.rutas.getServiciosRegistrados(id)
    .subscribe((data: any)=> {
      this.listaCategorias = data['categories'];
      console.log(this.listaCategorias);
    })
  }

  // crearFormularioCategoria(){
  //   this.formaCategoria = this.fb.group({
  //     id         : ['',],
  //     username   : ['',],
  //     name       : ['',],
  //     lastname   : [''],
  //     email      : ['',],
  //     phone      : ['',],
  //     about      : [''],
  //     experience : ['',],
  //     keyphrases : ['',],
  //     secret     : ['',],
  //     datebirth : [''],
  //     location  : this.fb.group({
  //       address   : ['',]
  //     }),
  //     categories : this.fb.group({
  //       id   : ['',]
  //     }),
  //   });
  // }

  // agregarCategoria(){

  // }
  registrarCategoria(){
    alert('Datos agregados');

    // this.rutas.putProvider(this.forma.value).subscribe(response =>{
    //   console.log(response);
      
    //   if(response != null){
    //     alert('Datos actualizados');
    //     //cerrar el modal
    //     //document.getElementById('cerrarModal').click();
    //     //document.getElementById('mostrarExito').click();
    //   }else{
    //     alert('Error!. Intentelo nuevamente');
    //   }
    // });

  }

}
