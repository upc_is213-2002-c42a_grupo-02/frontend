import { Component, OnInit } from '@angular/core';
import { RutasService } from 'src/app/services/rutas.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  listaServicios:any[] = [];
  usuarioLogin:any[] = [];
  usuarioLog;
  

  constructor(private rutas:RutasService) {}
  
  ngOnInit(): void {
    if(JSON.parse(localStorage.getItem('data')) != null){

      //this.tipoLogin = localStorage.getItem('tipo');
      this.usuarioLogin = JSON.parse(localStorage.getItem('data'));
      this.usuarioLog = this.usuarioLogin['username'];
    }else{
      //this.tipoLogin = null;
      this.usuarioLogin = [];
      this.usuarioLog = '';
    }

    this.listServiciosSolicitados(this.usuarioLog);

  }   

  listServiciosSolicitados(cliente: string){
    this.rutas.getServiciosUsuario(cliente)
    .subscribe((data: any)=> {
      this.listaServicios = data;
      console.log(this.listaServicios);
    })
  }
}
