import { Component, OnInit } from '@angular/core';
import { RutasService } from '../../services/rutas.service';

@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.scss']
})
export class ProvidersComponent implements OnInit {
  listaProviders: any[] = [];

  constructor(private rutas:RutasService) { }

  ngOnInit(): void {
    this.listProviders()
  }

  listProviders(){
    this.rutas.getProviders()
    .subscribe((data: any)=>{
      this.listaProviders = data
      console.log(this.listaProviders)
    });
  }

}


