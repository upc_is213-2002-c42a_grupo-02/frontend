import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryproviderComponent } from './categoryprovider.component';

describe('CategoryproviderComponent', () => {
  let component: CategoryproviderComponent;
  let fixture: ComponentFixture<CategoryproviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryproviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryproviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
