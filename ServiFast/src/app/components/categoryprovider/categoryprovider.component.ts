import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RutasService } from 'src/app/services/rutas.service';

@Component({
  selector: 'app-categoryprovider',
  templateUrl: './categoryprovider.component.html',
  styleUrls: ['./categoryprovider.component.scss']
})
export class CategoryproviderComponent implements OnInit {
  //serviciosSolicitar: any[] = [];
  jQuery:any;

  idProvider;
  listaCategoriasProvider: any[] = [];
  usuarioLogin:any[] = [];
  usuarioLog;
  tipoLogin: string;
  idCategoriaSolicitar;
  
  id;
  nombres;
  apellidos;
  cumpleanios;
  email;
  telefono;
  descripcion;
  experiencia;
  usuario;
  dataenvio:any[] = [];
  constructor(private rutas:RutasService, private route: ActivatedRoute) {
    
   }

  ngOnInit(): void {

    if(JSON.parse(localStorage.getItem('data')) != null){

      this.tipoLogin = localStorage.getItem('tipo');
      this.usuarioLogin = JSON.parse(localStorage.getItem('data'));
      this.usuarioLog = this.usuarioLogin['username'];
    }else{
      this.tipoLogin = null;
      this.usuarioLogin = [];
      this.usuarioLog = '';
    }



    this.idProvider = this.route.snapshot.paramMap.get("id");

    this.listCategoryProvider(this.idProvider);
    this.listDatosProvider(this.idProvider);
  }

  listCategoryProvider(id: number){
    this.rutas.getCategoryProvider(id)
    .subscribe((data: any)=> {
      this.listaCategoriasProvider = data['categories'];
      //console.log(this.listaCategoriasProvider)
    })
  }

  listDatosProvider(id: number){
    this.rutas.getCategoryProvider(id)
    .subscribe((data: any)=> {
      this.usuario = data['username'];
      this.nombres = data['name'];
      this.apellidos = data['lastname'];
      this.cumpleanios = data['datebirth'];
      this.email = data['email'];
      this.telefono = data['phone'];
      this.descripcion = data['about'];
      this.experiencia = data['experience'];
      //console.log( this.cumpleanios);
    })
  }

//solicitar servicio
  solicitar(categoria: string){
    this.idCategoriaSolicitar = categoria
  }

  cancelar(){
    this.idCategoriaSolicitar = '';
  }


  confirmarSolicitud(){
   
    this.dataenvio.push({
      category:{
        id:this.idCategoriaSolicitar 
      },
      provider:{
        username:this.usuario
      },
      user:{
        username:this.usuarioLogin['username']
      }
    })
    //console.log(this.dataenvio[0])
    
    this.rutas.postSolicitarServicio(this.dataenvio[0]).subscribe(response =>{
      console.log(response);
      if(response != null){
        //alert('Registro exitoso');
        this.dataenvio=[]; //limpiamos el obj
        //cerrar el modal
        document.getElementById('cerrarModal').click();
        document.getElementById('mostrarExito').click();
      }else{
        alert('Error...!');
      }
     
    });
   }

}
