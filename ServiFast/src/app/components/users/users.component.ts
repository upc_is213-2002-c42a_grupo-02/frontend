import { Component, OnInit } from '@angular/core';
import { RutasService } from '../../services/rutas.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  listaUsuarios: any[] = [];

  constructor(private rutas:RutasService) { }

  ngOnInit(): void {
    this.listarUsuario()
  }

  listarUsuario(){
    this.rutas.getUsuarios()
    .subscribe((data: any)=>{
      this.listaUsuarios = data
      //console.log(this.listaUsuarios)
    });
  }

}



