import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RutasService } from 'src/app/services/rutas.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,private rutas:RutasService) { }

  ngOnInit(): void {
  }

  login(form: NgForm){
    //console.log(form.value);
    if(form.value.tipoUser==="option1"){
      this.loginUsers(form.value); 
    }else{
      this.loginProveedor(form.value)
    }
  }

  loginUsers(login:any){
    this.rutas.postUserLogin(login).subscribe(data=>{
      console.log(data)
         if(data.username===""){
            console.log("error en logeo")
         }else{
          //  data.setItem({
          //    tipo: 'cliente'
          //  })
          localStorage.setItem('data',JSON.stringify(data) );
          localStorage.setItem('tipo','cliente');
          //console.log(localStorage.setItem('data',JSON.stringify(data) ));
          this.rutas.verMenu(true)
          //window.location.reload();
          this.router.navigate(['/index']);
          
         }
    }) 
  }
  loginProveedor(login:any){
    this.rutas.postProveedotLogin(login).subscribe(data=>{
      console.log(data)
         if(data.username===""){
            console.log("erro en logeo")
         }else{
          localStorage.setItem('data',JSON.stringify(data) );
          localStorage.setItem('tipo','proveedor');
          //console.log(localStorage)
          this.router.navigate(['/principal']);
          
         }
    }) 
  }

  // reloadPage(){
  //   window.location.reload();
  // }


}
