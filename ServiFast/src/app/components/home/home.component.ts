import { Component, OnInit } from '@angular/core';
import { RutasService } from 'src/app/services/rutas.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  listaCategories: any[] = [];
  //listaUsuarios: any[] = [];
  listaProviders: any[] = [];

  constructor(private rutas:RutasService) { }

  ngOnInit(): void {
    this.listarCategories();
    this.listarProviders();
  }

  listarCategories(){
    this.rutas.getCategories()
    .subscribe((data: any)=> {
      this.listaCategories = data
      //console.log(this.listaCategories)
    });
  }

  // listarUsers(){
  //   this.rutas.getUsuarios()
  //   .subscribe((data: any)=>{
  //     this.listaUsuarios = data
  //     //console.log(this.listaUsuarios)
  //   });
  // }

  listarProviders(){
    this.rutas.getProviders()
    .subscribe((data: any)=>{
      this.listaProviders = data
      //console.log(this.listaUsuarios)
    });
  }

}
