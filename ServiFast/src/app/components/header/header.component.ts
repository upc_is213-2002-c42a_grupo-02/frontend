import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RutasService } from 'src/app/services/rutas.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  logeado: boolean;
  usuarioLogin:any[] = [];

  usuario: string;
  tipoLogin: string;

  constructor(private router: Router,private rutas: RutasService) {
    
   
   }

  ngOnInit(): void {
    
   
    //console.log(JSON.parse(localStorage.getItem('data')));
    
    if (JSON.parse(localStorage.getItem('data')) === null){
       //console.log('no esta logeado'); 
      this.logeado = false;
    } else{
      this.logeado = true;
      this.usuarioLogin = JSON.parse(localStorage.getItem('data'));
      this.usuario = this.usuarioLogin['username'];
      this.tipoLogin = localStorage.getItem('tipo');
      
      //window.location.reload();
      // this.rutas.currentData.subscribe((res)=>{
      //   this.logeado = res;
        
      // }) 
      // console.log('si esta logeado');
    }
  }

  logout(){
    localStorage.removeItem('data');
    localStorage.removeItem('tipo');
    this.router.navigate(['/home']);
    this.logeado = false;
  }

//   reloadPage() {
//     window.location.reload();
// }

}
