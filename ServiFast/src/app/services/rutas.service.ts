import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable,BehaviorSubject} from 'rxjs'; 

@Injectable({
  providedIn: 'root'
})

export class RutasService {
  private dataSurce= new BehaviorSubject<boolean>(true)
  currentData=this.dataSurce.asObservable();

  constructor(private http: HttpClient) {}

  verMenu(any){
    this.dataSurce.next(any)
 }
  url(query: string) {
    const url = `http://localhost:8080/api/${query}`;
    return this.http.get(url);
  }
  // OBTENER USUARIOS
  getUsuarios() {
    return this.url('users');
  }
  // OBTENER PROVEEDORES
  getProviders() {
    return this.url('providers');
  }
  // OBTENER CATEGORIAS Y DATOS DEL PROVEEDOR POR ID
  getCategoryProvider(id: number) {
    return this.url('provider/' + id);
  }

   // OBTENER CATEGORIAS
   getCategories() {
    return this.url('categories');
  }
  // OBTENER SERVICIOS POR USUARIO
  getServiciosUsuario(cliente: string) {
    return this.url('requests/user/' + cliente);
  }
  // OBTENER categorias por proveedor
  getServiciosRegistrados(id: number) {
    return this.url('provider/' + id);
  }

  postUsuario(persona: any): Observable<any> {
 
    let url = "http://localhost:8080/api/user"
    return this.http.post<any>(url, persona);
  }

  //CREAR PROVEEDOR
  postProvider(proveedor: any): Observable<any> {
 
    let url = "http://localhost:8080/api/provider"
    return this.http.post<any>(url, proveedor);
  }
  //ACTUALIZAR PROVEEDOR
  putProvider(proveedor: any): Observable<any> {
 
    let url = "http://localhost:8080/api/provider"
    return this.http.put<any>(url, proveedor);
  }

  //LOGIN
  postUserLogin(login: any): Observable<any> {
 
    let url = "http://localhost:8080/api/user/login"
    return this.http.post<any>(url, login);

    
  }
  postProveedotLogin(login: any): Observable<any> {
 
    let url = "http://localhost:8080/api/provider/login"
    return this.http.post<any>(url, login);
  }

  //SOLICITAR SERVICIO PROVEEDOR
  postSolicitarServicio(datos: any): Observable<any> {
    let url = "http://localhost:8080/api/request"
    return this.http.post<any>(url, datos);
  }
  
}




